package net.feed.fdeintegration.client;

import java.util.List;

import net.feed.fdeintegration.client.api.AuthenticationApi;
import net.feed.fdeintegration.client.api.FeedentityOrganizationsApi;
import net.feed.fdeintegration.client.model.JWTToken;
import net.feed.fdeintegration.client.model.LoginVM;
import net.feed.fdeintegration.client.model.OrganizationDTO;

public class Test {
	
	public static void main(String[] args) {
		
		try {
			ApiClient defaultClient = Configuration.getDefaultApiClient();
			defaultClient.setBasePath("http://18.194.178.129:8080/fde-integrations");
//			defaultClient.setDebugging(true);
			
			AuthenticationApi authenticationApi = new AuthenticationApi(defaultClient);
			LoginVM loginVM = new LoginVM();
			loginVM.setUsername("user");
			loginVM.setPassword("user1234");
			loginVM.setRememberMe(false);
			JWTToken token = authenticationApi.authorizeUsingPOST(loginVM);
			System.out.println(token);
			
			defaultClient.addDefaultHeader("Authorization", "Bearer " + token.getIdToken());
			FeedentityOrganizationsApi feedentityOrganizationsApi = new FeedentityOrganizationsApi(defaultClient);
			List<OrganizationDTO> orgs = feedentityOrganizationsApi.getAllOrganizationByAuthKeyUsingGET("TEST");
			
			for (OrganizationDTO org: orgs) {
				System.out.println(org);
			}
			
		} catch (ApiException e) {
			e.printStackTrace();
		}
	}
}
