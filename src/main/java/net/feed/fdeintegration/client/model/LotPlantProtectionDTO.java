/*
 * FdeIntegrations API
 * FdeIntegrations API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package net.feed.fdeintegration.client.model;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.time.LocalTime;

import org.joda.time.LocalDate;

/**
 * LotPlantProtectionDTO
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-10T09:51:34.939+01:00")
public class LotPlantProtectionDTO {
  @SerializedName("adversityId")
  private Long adversityId = null;

  @SerializedName("adversityName")
  private String adversityName = null;

  @SerializedName("adversityScientificName")
  private String adversityScientificName = null;

  @SerializedName("appMethod")
  private String appMethod = null;

  @SerializedName("date")
  private LocalDate date = null;

  @SerializedName("failureTime")
  private Integer failureTime = null;

  @SerializedName("id")
  private Long id = null;

  @SerializedName("mixDistribuited")
  private Double mixDistribuited = null;

  @SerializedName("operatorFullName")
  private String operatorFullName = null;

  @SerializedName("operatorId")
  private Long operatorId = null;

  @SerializedName("phenologicalPhase")
  private String phenologicalPhase = null;

  @SerializedName("productId")
  private Long productId = null;

  @SerializedName("productName")
  private String productName = null;

  @SerializedName("quantity")
  private Double quantity = null;

  @SerializedName("returnPeriod")
  private Integer returnPeriod = null;

  @SerializedName("start")
  private LocalTime start = null;

  @SerializedName("stop")
  private LocalTime stop = null;

  @SerializedName("udm")
  private String udm = null;

  @SerializedName("weather")
  private String weather = null;

  public LotPlantProtectionDTO adversityId(Long adversityId) {
    this.adversityId = adversityId;
    return this;
  }

   /**
   * Get adversityId
   * @return adversityId
  **/
  @ApiModelProperty(value = "")
  public Long getAdversityId() {
    return adversityId;
  }

  public void setAdversityId(Long adversityId) {
    this.adversityId = adversityId;
  }

  public LotPlantProtectionDTO adversityName(String adversityName) {
    this.adversityName = adversityName;
    return this;
  }

   /**
   * Get adversityName
   * @return adversityName
  **/
  @ApiModelProperty(value = "")
  public String getAdversityName() {
    return adversityName;
  }

  public void setAdversityName(String adversityName) {
    this.adversityName = adversityName;
  }

  public LotPlantProtectionDTO adversityScientificName(String adversityScientificName) {
    this.adversityScientificName = adversityScientificName;
    return this;
  }

   /**
   * Get adversityScientificName
   * @return adversityScientificName
  **/
  @ApiModelProperty(value = "")
  public String getAdversityScientificName() {
    return adversityScientificName;
  }

  public void setAdversityScientificName(String adversityScientificName) {
    this.adversityScientificName = adversityScientificName;
  }

  public LotPlantProtectionDTO appMethod(String appMethod) {
    this.appMethod = appMethod;
    return this;
  }

   /**
   * Get appMethod
   * @return appMethod
  **/
  @ApiModelProperty(value = "")
  public String getAppMethod() {
    return appMethod;
  }

  public void setAppMethod(String appMethod) {
    this.appMethod = appMethod;
  }

  public LotPlantProtectionDTO date(LocalDate date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @ApiModelProperty(value = "")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public LotPlantProtectionDTO failureTime(Integer failureTime) {
    this.failureTime = failureTime;
    return this;
  }

   /**
   * Get failureTime
   * @return failureTime
  **/
  @ApiModelProperty(value = "")
  public Integer getFailureTime() {
    return failureTime;
  }

  public void setFailureTime(Integer failureTime) {
    this.failureTime = failureTime;
  }

  public LotPlantProtectionDTO id(Long id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LotPlantProtectionDTO mixDistribuited(Double mixDistribuited) {
    this.mixDistribuited = mixDistribuited;
    return this;
  }

   /**
   * Get mixDistribuited
   * @return mixDistribuited
  **/
  @ApiModelProperty(value = "")
  public Double getMixDistribuited() {
    return mixDistribuited;
  }

  public void setMixDistribuited(Double mixDistribuited) {
    this.mixDistribuited = mixDistribuited;
  }

  public LotPlantProtectionDTO operatorFullName(String operatorFullName) {
    this.operatorFullName = operatorFullName;
    return this;
  }

   /**
   * Get operatorFullName
   * @return operatorFullName
  **/
  @ApiModelProperty(value = "")
  public String getOperatorFullName() {
    return operatorFullName;
  }

  public void setOperatorFullName(String operatorFullName) {
    this.operatorFullName = operatorFullName;
  }

  public LotPlantProtectionDTO operatorId(Long operatorId) {
    this.operatorId = operatorId;
    return this;
  }

   /**
   * Get operatorId
   * @return operatorId
  **/
  @ApiModelProperty(value = "")
  public Long getOperatorId() {
    return operatorId;
  }

  public void setOperatorId(Long operatorId) {
    this.operatorId = operatorId;
  }

  public LotPlantProtectionDTO phenologicalPhase(String phenologicalPhase) {
    this.phenologicalPhase = phenologicalPhase;
    return this;
  }

   /**
   * Get phenologicalPhase
   * @return phenologicalPhase
  **/
  @ApiModelProperty(value = "")
  public String getPhenologicalPhase() {
    return phenologicalPhase;
  }

  public void setPhenologicalPhase(String phenologicalPhase) {
    this.phenologicalPhase = phenologicalPhase;
  }

  public LotPlantProtectionDTO productId(Long productId) {
    this.productId = productId;
    return this;
  }

   /**
   * Get productId
   * @return productId
  **/
  @ApiModelProperty(value = "")
  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public LotPlantProtectionDTO productName(String productName) {
    this.productName = productName;
    return this;
  }

   /**
   * Get productName
   * @return productName
  **/
  @ApiModelProperty(value = "")
  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public LotPlantProtectionDTO quantity(Double quantity) {
    this.quantity = quantity;
    return this;
  }

   /**
   * Get quantity
   * @return quantity
  **/
  @ApiModelProperty(value = "")
  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public LotPlantProtectionDTO returnPeriod(Integer returnPeriod) {
    this.returnPeriod = returnPeriod;
    return this;
  }

   /**
   * Get returnPeriod
   * @return returnPeriod
  **/
  @ApiModelProperty(value = "")
  public Integer getReturnPeriod() {
    return returnPeriod;
  }

  public void setReturnPeriod(Integer returnPeriod) {
    this.returnPeriod = returnPeriod;
  }

  public LotPlantProtectionDTO start(LocalTime start) {
    this.start = start;
    return this;
  }

   /**
   * Get start
   * @return start
  **/
  @ApiModelProperty(value = "")
  public LocalTime getStart() {
    return start;
  }

  public void setStart(LocalTime start) {
    this.start = start;
  }

  public LotPlantProtectionDTO stop(LocalTime stop) {
    this.stop = stop;
    return this;
  }

   /**
   * Get stop
   * @return stop
  **/
  @ApiModelProperty(value = "")
  public LocalTime getStop() {
    return stop;
  }

  public void setStop(LocalTime stop) {
    this.stop = stop;
  }

  public LotPlantProtectionDTO udm(String udm) {
    this.udm = udm;
    return this;
  }

   /**
   * Get udm
   * @return udm
  **/
  @ApiModelProperty(value = "")
  public String getUdm() {
    return udm;
  }

  public void setUdm(String udm) {
    this.udm = udm;
  }

  public LotPlantProtectionDTO weather(String weather) {
    this.weather = weather;
    return this;
  }

   /**
   * Get weather
   * @return weather
  **/
  @ApiModelProperty(value = "")
  public String getWeather() {
    return weather;
  }

  public void setWeather(String weather) {
    this.weather = weather;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotPlantProtectionDTO lotPlantProtectionDTO = (LotPlantProtectionDTO) o;
    return Objects.equals(this.adversityId, lotPlantProtectionDTO.adversityId) &&
        Objects.equals(this.adversityName, lotPlantProtectionDTO.adversityName) &&
        Objects.equals(this.adversityScientificName, lotPlantProtectionDTO.adversityScientificName) &&
        Objects.equals(this.appMethod, lotPlantProtectionDTO.appMethod) &&
        Objects.equals(this.date, lotPlantProtectionDTO.date) &&
        Objects.equals(this.failureTime, lotPlantProtectionDTO.failureTime) &&
        Objects.equals(this.id, lotPlantProtectionDTO.id) &&
        Objects.equals(this.mixDistribuited, lotPlantProtectionDTO.mixDistribuited) &&
        Objects.equals(this.operatorFullName, lotPlantProtectionDTO.operatorFullName) &&
        Objects.equals(this.operatorId, lotPlantProtectionDTO.operatorId) &&
        Objects.equals(this.phenologicalPhase, lotPlantProtectionDTO.phenologicalPhase) &&
        Objects.equals(this.productId, lotPlantProtectionDTO.productId) &&
        Objects.equals(this.productName, lotPlantProtectionDTO.productName) &&
        Objects.equals(this.quantity, lotPlantProtectionDTO.quantity) &&
        Objects.equals(this.returnPeriod, lotPlantProtectionDTO.returnPeriod) &&
        Objects.equals(this.start, lotPlantProtectionDTO.start) &&
        Objects.equals(this.stop, lotPlantProtectionDTO.stop) &&
        Objects.equals(this.udm, lotPlantProtectionDTO.udm) &&
        Objects.equals(this.weather, lotPlantProtectionDTO.weather);
  }

  @Override
  public int hashCode() {
    return Objects.hash(adversityId, adversityName, adversityScientificName, appMethod, date, failureTime, id, mixDistribuited, operatorFullName, operatorId, phenologicalPhase, productId, productName, quantity, returnPeriod, start, stop, udm, weather);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotPlantProtectionDTO {\n");
    
    sb.append("    adversityId: ").append(toIndentedString(adversityId)).append("\n");
    sb.append("    adversityName: ").append(toIndentedString(adversityName)).append("\n");
    sb.append("    adversityScientificName: ").append(toIndentedString(adversityScientificName)).append("\n");
    sb.append("    appMethod: ").append(toIndentedString(appMethod)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    failureTime: ").append(toIndentedString(failureTime)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    mixDistribuited: ").append(toIndentedString(mixDistribuited)).append("\n");
    sb.append("    operatorFullName: ").append(toIndentedString(operatorFullName)).append("\n");
    sb.append("    operatorId: ").append(toIndentedString(operatorId)).append("\n");
    sb.append("    phenologicalPhase: ").append(toIndentedString(phenologicalPhase)).append("\n");
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    productName: ").append(toIndentedString(productName)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    returnPeriod: ").append(toIndentedString(returnPeriod)).append("\n");
    sb.append("    start: ").append(toIndentedString(start)).append("\n");
    sb.append("    stop: ").append(toIndentedString(stop)).append("\n");
    sb.append("    udm: ").append(toIndentedString(udm)).append("\n");
    sb.append("    weather: ").append(toIndentedString(weather)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

