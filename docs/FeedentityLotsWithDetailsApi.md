# FeedentityLotsWithDetailsApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLotDetailsUsingGET**](FeedentityLotsWithDetailsApi.md#getLotDetailsUsingGET) | **GET** /api/v1/orgs/{orgId}/lot/{lotId} | Finds a single lot with all details
[**getOrganizationLotForPuUsingGET**](FeedentityLotsWithDetailsApi.md#getOrganizationLotForPuUsingGET) | **GET** /api/v1/orgs/{orgId}/pus/{puId} | Finds all lots by organization and production unit


<a name="getLotDetailsUsingGET"></a>
# **getLotDetailsUsingGET**
> LotDTO getLotDetailsUsingGET(orgId, lotId)

Finds a single lot with all details

Single lot object with all informations about production unit associated and list of raisings, plant protections, fertilizations and irrigations.

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.FeedentityLotsWithDetailsApi;


FeedentityLotsWithDetailsApi apiInstance = new FeedentityLotsWithDetailsApi();
Long orgId = 789L; // Long | orgId
String lotId = "lotId_example"; // String | lotId
try {
    LotDTO result = apiInstance.getLotDetailsUsingGET(orgId, lotId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FeedentityLotsWithDetailsApi#getLotDetailsUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgId** | **Long**| orgId |
 **lotId** | **String**| lotId |

### Return type

[**LotDTO**](LotDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getOrganizationLotForPuUsingGET"></a>
# **getOrganizationLotForPuUsingGET**
> LotForPuDTO getOrganizationLotForPuUsingGET(orgId, puId)

Finds all lots by organization and production unit

List of all lot for an organization and production unit

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.FeedentityLotsWithDetailsApi;


FeedentityLotsWithDetailsApi apiInstance = new FeedentityLotsWithDetailsApi();
Long orgId = 789L; // Long | orgId
Long puId = 789L; // Long | puId
try {
    LotForPuDTO result = apiInstance.getOrganizationLotForPuUsingGET(orgId, puId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FeedentityLotsWithDetailsApi#getOrganizationLotForPuUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgId** | **Long**| orgId |
 **puId** | **Long**| puId |

### Return type

[**LotForPuDTO**](LotForPuDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

