
# OrganizationDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**productionUnits** | [**List&lt;OrganizationPuDTO&gt;**](OrganizationPuDTO.md) |  |  [optional]



