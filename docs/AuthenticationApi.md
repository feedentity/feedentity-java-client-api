# AuthenticationApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorizeUsingPOST**](AuthenticationApi.md#authorizeUsingPOST) | **POST** /api/authenticate | Get Authentication Token


<a name="authorizeUsingPOST"></a>
# **authorizeUsingPOST**
> JWTToken authorizeUsingPOST(loginVM)

Get Authentication Token

Get the JWT token for authentication

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.AuthenticationApi;


AuthenticationApi apiInstance = new AuthenticationApi();
LoginVM loginVM = new LoginVM(); // LoginVM | loginVM
try {
    JWTToken result = apiInstance.authorizeUsingPOST(loginVM);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#authorizeUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loginVM** | [**LoginVM**](LoginVM.md)| loginVM |

### Return type

[**JWTToken**](JWTToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

