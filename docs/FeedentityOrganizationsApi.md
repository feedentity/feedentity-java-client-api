# FeedentityOrganizationsApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllOrganizationByAuthKeyUsingGET**](FeedentityOrganizationsApi.md#getAllOrganizationByAuthKeyUsingGET) | **GET** /api/v1/orgs/{authKey}/pus | Finds organization by auth key


<a name="getAllOrganizationByAuthKeyUsingGET"></a>
# **getAllOrganizationByAuthKeyUsingGET**
> List&lt;OrganizationDTO&gt; getAllOrganizationByAuthKeyUsingGET(authKey)

Finds organization by auth key

List of all organization defined by auth key with production units

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.FeedentityOrganizationsApi;


FeedentityOrganizationsApi apiInstance = new FeedentityOrganizationsApi();
String authKey = "authKey_example"; // String | authKey
try {
    List<OrganizationDTO> result = apiInstance.getAllOrganizationByAuthKeyUsingGET(authKey);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FeedentityOrganizationsApi#getAllOrganizationByAuthKeyUsingGET");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authKey** | **String**| authKey |

### Return type

[**List&lt;OrganizationDTO&gt;**](OrganizationDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

