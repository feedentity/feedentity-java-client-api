
# LotDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**closureDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**code** | **String** |  |  [optional]
**cropId** | **Long** |  |  [optional]
**cropName** | **String** |  |  [optional]
**cropScientificName** | **String** |  |  [optional]
**cropType** | **String** |  |  [optional]
**cropVarietyId** | **Long** |  |  [optional]
**cropVarietyName** | **String** |  |  [optional]
**fertilizers** | [**List&lt;LotFertilizerDTO&gt;**](LotFertilizerDTO.md) |  |  [optional]
**gage** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**irrigations** | [**List&lt;LotIrrigationDTO&gt;**](LotIrrigationDTO.md) |  |  [optional]
**lotType** | **String** |  |  [optional]
**plantProtections** | [**List&lt;LotPlantProtectionDTO&gt;**](LotPlantProtectionDTO.md) |  |  [optional]
**productionUnits** | [**List&lt;OrganizationPuDTO&gt;**](OrganizationPuDTO.md) |  |  [optional]
**raisings** | [**List&lt;LotRaisingDTO&gt;**](LotRaisingDTO.md) |  |  [optional]
**seedTreatment** | **String** |  |  [optional]
**seedlingPassport** | **String** |  |  [optional]
**size** | **Double** |  |  [optional]
**sowingDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**sowingDensity** | **Double** |  |  [optional]
**totalSeeds** | **Double** |  |  [optional]



