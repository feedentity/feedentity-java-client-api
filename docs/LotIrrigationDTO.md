
# LotIrrigationDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appMethod** | **String** |  |  [optional]
**date** | [**LocalDate**](LocalDate.md) |  |  [optional]
**distributedQty** | **Double** |  |  [optional]
**id** | **Long** |  |  [optional]
**irrigationSystemId** | **Long** |  |  [optional]
**irrigationSystemName** | **String** |  |  [optional]
**irrigatorFlow** | **Double** |  |  [optional]
**irrigatorsCount** | **Integer** |  |  [optional]
**jetDistance** | **Double** |  |  [optional]
**length** | **Double** |  |  [optional]
**lines** | **Integer** |  |  [optional]
**lotPuDTO** | [**LotPuDTO**](LotPuDTO.md) |  |  [optional]
**operatorFullName** | **String** |  |  [optional]
**operatorId** | **Long** |  |  [optional]
**phenologicalPhase** | **String** |  |  [optional]
**start** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**stop** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**weather** | **String** |  |  [optional]



