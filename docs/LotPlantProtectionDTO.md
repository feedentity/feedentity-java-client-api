
# LotPlantProtectionDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adversityId** | **Long** |  |  [optional]
**adversityName** | **String** |  |  [optional]
**adversityScientificName** | **String** |  |  [optional]
**appMethod** | **String** |  |  [optional]
**date** | [**LocalDate**](LocalDate.md) |  |  [optional]
**failureTime** | **Integer** |  |  [optional]
**id** | **Long** |  |  [optional]
**mixDistribuited** | **Double** |  |  [optional]
**operatorFullName** | **String** |  |  [optional]
**operatorId** | **Long** |  |  [optional]
**phenologicalPhase** | **String** |  |  [optional]
**productId** | **Long** |  |  [optional]
**productName** | **String** |  |  [optional]
**quantity** | **Double** |  |  [optional]
**returnPeriod** | **Integer** |  |  [optional]
**start** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**stop** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**udm** | **String** |  |  [optional]
**weather** | **String** |  |  [optional]



