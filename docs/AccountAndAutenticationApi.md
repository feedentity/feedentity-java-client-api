# AccountAndAutenticationApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changePasswordUsingPOST**](AccountAndAutenticationApi.md#changePasswordUsingPOST) | **POST** /api/account/change-password | Change password
[**getAccountUsingGET**](AccountAndAutenticationApi.md#getAccountUsingGET) | **GET** /api/account | Get account informations
[**isAuthenticatedUsingGET**](AccountAndAutenticationApi.md#isAuthenticatedUsingGET) | **GET** /api/authenticate | Verify if user is authenticate
[**saveAccountUsingPOST**](AccountAndAutenticationApi.md#saveAccountUsingPOST) | **POST** /api/account | Update account informations


<a name="changePasswordUsingPOST"></a>
# **changePasswordUsingPOST**
> changePasswordUsingPOST(password)

Change password

To change account passord

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.AccountAndAutenticationApi;


AccountAndAutenticationApi apiInstance = new AccountAndAutenticationApi();
String password = "password_example"; // String | password
try {
    apiInstance.changePasswordUsingPOST(password);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountAndAutenticationApi#changePasswordUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **String**| password |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="getAccountUsingGET"></a>
# **getAccountUsingGET**
> UserDTO getAccountUsingGET()

Get account informations

Return an object with current user informations

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.AccountAndAutenticationApi;


AccountAndAutenticationApi apiInstance = new AccountAndAutenticationApi();
try {
    UserDTO result = apiInstance.getAccountUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountAndAutenticationApi#getAccountUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDTO**](UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="isAuthenticatedUsingGET"></a>
# **isAuthenticatedUsingGET**
> String isAuthenticatedUsingGET()

Verify if user is authenticate

If user is authenticate returns 200

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.AccountAndAutenticationApi;


AccountAndAutenticationApi apiInstance = new AccountAndAutenticationApi();
try {
    String result = apiInstance.isAuthenticatedUsingGET();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountAndAutenticationApi#isAuthenticatedUsingGET");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

<a name="saveAccountUsingPOST"></a>
# **saveAccountUsingPOST**
> saveAccountUsingPOST(userDTO)

Update account informations

To save user account informations

### Example
```java
// Import classes:
//import net.feed.fdeintegration.client.ApiException;
//import net.feed.fdeintegration.client.api.AccountAndAutenticationApi;


AccountAndAutenticationApi apiInstance = new AccountAndAutenticationApi();
UserDTO userDTO = new UserDTO(); // UserDTO | userDTO
try {
    apiInstance.saveAccountUsingPOST(userDTO);
} catch (ApiException e) {
    System.err.println("Exception when calling AccountAndAutenticationApi#saveAccountUsingPOST");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDTO** | [**UserDTO**](UserDTO.md)| userDTO |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

