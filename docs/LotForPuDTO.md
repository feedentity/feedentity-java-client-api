
# LotForPuDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lots** | [**List&lt;LotDTO&gt;**](LotDTO.md) |  |  [optional]
**organizationId** | **Long** |  |  [optional]
**organizationName** | **String** |  |  [optional]
**productionUnitDimension** | **Double** |  |  [optional]
**productionUnitId** | **Long** |  |  [optional]
**productionUnitName** | **String** |  |  [optional]



