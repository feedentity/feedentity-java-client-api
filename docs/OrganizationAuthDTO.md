
# OrganizationAuthDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activated** | **Boolean** |  | 
**authKey** | **String** |  | 
**id** | **Long** |  |  [optional]
**note** | **String** |  |  [optional]
**organizationId** | **Long** |  | 
**userId** | **Long** |  |  [optional]
**userLogin** | **String** |  |  [optional]



