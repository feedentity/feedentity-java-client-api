
# OrganizationPuPartDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**catastalPart** | **String** |  |  [optional]
**catastalSheet** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**length** | **Double** |  |  [optional]
**sections** | **Integer** |  |  [optional]
**size** | **Double** |  |  [optional]
**width** | **Double** |  |  [optional]



