
# LotFertilizerDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appMethod** | **String** |  |  [optional]
**b** | **Double** |  |  [optional]
**ca** | **Double** |  |  [optional]
**cu** | **Double** |  |  [optional]
**date** | [**LocalDate**](LocalDate.md) |  |  [optional]
**density** | **Double** |  |  [optional]
**fe** | **Double** |  |  [optional]
**id** | **Long** |  |  [optional]
**k** | **Double** |  |  [optional]
**mg** | **Double** |  |  [optional]
**mixDistribuited** | **Double** |  |  [optional]
**mn** | **Double** |  |  [optional]
**mo** | **Double** |  |  [optional]
**n** | **Double** |  |  [optional]
**operatorFullName** | **String** |  |  [optional]
**operatorId** | **Long** |  |  [optional]
**p** | **Double** |  |  [optional]
**ph** | **Double** |  |  [optional]
**phenologicalPhase** | **String** |  |  [optional]
**product** | **String** |  |  [optional]
**quantity** | **Double** |  |  [optional]
**s** | **Double** |  |  [optional]
**start** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**stop** | [**org.joda.time.***](org.joda.time.*.md) |  |  [optional]
**udm** | **String** |  |  [optional]
**weather** | **String** |  |  [optional]
**zn** | **Double** |  |  [optional]



