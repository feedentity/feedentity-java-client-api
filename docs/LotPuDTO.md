
# LotPuDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**organizationPuDTO** | [**OrganizationPuDTO**](OrganizationPuDTO.md) |  |  [optional]
**size** | **Double** |  |  [optional]



