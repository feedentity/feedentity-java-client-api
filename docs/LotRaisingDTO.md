
# LotRaisingDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**endingDate** | [**LocalDate**](LocalDate.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**startingDate** | [**LocalDate**](LocalDate.md) |  |  [optional]



