
# OrganizationPuDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dimension** | **Double** |  |  [optional]
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**productionUnitParts** | [**List&lt;OrganizationPuPartDTO&gt;**](OrganizationPuPartDTO.md) |  |  [optional]
**type** | **String** |  |  [optional]



