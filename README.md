# feedentity-java-client-api

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>net.feed.fdeintegration.client</groupId>
    <artifactId>feedentity-java-client-api</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "net.feed.fdeintegration.client:feedentity-java-client-api:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/feedentity-java-client-api-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import net.feed.fdeintegration.client.*;
import net.feed.fdeintegration.client.auth.*;
import net.feed.fdeintegration.client.model.*;
import net.feed.fdeintegration.client.api.AccountAndAutenticationApi;

import java.io.File;
import java.util.*;

public class AccountAndAutenticationApiExample {

    public static void main(String[] args) {
        
        AccountAndAutenticationApi apiInstance = new AccountAndAutenticationApi();
        String password = "password_example"; // String | password
        try {
            apiInstance.changePasswordUsingPOST(password);
        } catch (ApiException e) {
            System.err.println("Exception when calling AccountAndAutenticationApi#changePasswordUsingPOST");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AccountAndAutenticationApi* | [**changePasswordUsingPOST**](docs/AccountAndAutenticationApi.md#changePasswordUsingPOST) | **POST** /api/account/change-password | Change password
*AccountAndAutenticationApi* | [**getAccountUsingGET**](docs/AccountAndAutenticationApi.md#getAccountUsingGET) | **GET** /api/account | Get account informations
*AccountAndAutenticationApi* | [**isAuthenticatedUsingGET**](docs/AccountAndAutenticationApi.md#isAuthenticatedUsingGET) | **GET** /api/authenticate | Verify if user is authenticate
*AccountAndAutenticationApi* | [**saveAccountUsingPOST**](docs/AccountAndAutenticationApi.md#saveAccountUsingPOST) | **POST** /api/account | Update account informations
*AuthenticationApi* | [**authorizeUsingPOST**](docs/AuthenticationApi.md#authorizeUsingPOST) | **POST** /api/authenticate | Get Authentication Token
*FeedentityLotsWithDetailsApi* | [**getLotDetailsUsingGET**](docs/FeedentityLotsWithDetailsApi.md#getLotDetailsUsingGET) | **GET** /api/v1/orgs/{orgId}/lot/{lotId} | Finds a single lot with all details
*FeedentityLotsWithDetailsApi* | [**getOrganizationLotForPuUsingGET**](docs/FeedentityLotsWithDetailsApi.md#getOrganizationLotForPuUsingGET) | **GET** /api/v1/orgs/{orgId}/pus/{puId} | Finds all lots by organization and production unit
*FeedentityOrganizationsApi* | [**getAllOrganizationByAuthKeyUsingGET**](docs/FeedentityOrganizationsApi.md#getAllOrganizationByAuthKeyUsingGET) | **GET** /api/v1/orgs/{authKey}/pus | Finds organization by auth key


## Documentation for Models

 - [JWTToken](docs/JWTToken.md)
 - [KeyAndPasswordVM](docs/KeyAndPasswordVM.md)
 - [LoginVM](docs/LoginVM.md)
 - [LotDTO](docs/LotDTO.md)
 - [LotFertilizerDTO](docs/LotFertilizerDTO.md)
 - [LotForPuDTO](docs/LotForPuDTO.md)
 - [LotIrrigationDTO](docs/LotIrrigationDTO.md)
 - [LotPlantProtectionDTO](docs/LotPlantProtectionDTO.md)
 - [LotPuDTO](docs/LotPuDTO.md)
 - [LotRaisingDTO](docs/LotRaisingDTO.md)
 - [ManagedUserVM](docs/ManagedUserVM.md)
 - [OrganizationAuthDTO](docs/OrganizationAuthDTO.md)
 - [OrganizationDTO](docs/OrganizationDTO.md)
 - [OrganizationPuDTO](docs/OrganizationPuDTO.md)
 - [OrganizationPuPartDTO](docs/OrganizationPuPartDTO.md)
 - [ProfileInfoVM](docs/ProfileInfoVM.md)
 - [User](docs/User.md)
 - [UserDTO](docs/UserDTO.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



